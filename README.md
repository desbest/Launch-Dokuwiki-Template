# Launch dokuwiki template

* Designed by [Justin at Audain Designs](http://themes.audaindesigns.com)
* Converted by [desbest](http://desbest.com)
* Metadata is in template.info.txt
* Under the GPL license (see copying file)
* [More information](http://dokuwiki.org/template:launch)

![launch theme screenshot](https://i.imgur.com/mpmBHdy.png)