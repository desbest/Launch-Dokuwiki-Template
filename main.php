<?php
/**
 * Launch Starter Template
 * Based on the starter template
 *
 * @link     http://dokuwiki.org/template:launch
 * @author   desbest <afaninthehouse@gmail.com>
 * @license  GPL 2 (http://www.gnu.org/licenses/gpl.html)
 */

if (!defined('DOKU_INC')) die(); /* must be run from within DokuWiki */
@require_once(dirname(__FILE__).'/tpl_functions.php'); /* include hook for template functions */
header('X-UA-Compatible: IE=edge,chrome=1');

$showTools = !tpl_getConf('hideTools') || ( tpl_getConf('hideTools') && !empty($_SERVER['REMOTE_USER']) );
$showSidebar = page_findnearest($conf['sidebar']) && ($ACT=='show');
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $conf['lang'] ?>"
  lang="<?php echo $conf['lang'] ?>" dir="<?php echo $lang['direction'] ?>" class="no-js">
<head>
    <meta charset="UTF-8" />
    <title><?php tpl_pagetitle() ?> [<?php echo strip_tags($conf['title']) ?>]</title>
    <script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>
    <?php tpl_metaheaders() ?>
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <?php echo tpl_favicon(array('favicon', 'mobile')) ?>
    <?php tpl_includeFile('meta.html') ?>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Fonts-->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    <script defer type="text/javascript" src="<?php echo tpl_basedir();?>/js/mobileinputsize.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="site <?php echo tpl_classes(); ?> <?php echo ($showSidebar) ? 'hasSidebar' : ''; ?>">

    <?php tpl_includeFile('header.html') ?>
    <!--header-->
    <header id="dokuwiki__top" class="header">
        <div class="container">
            <div class="row">
                <div class="goal-summary pull-left">
                    <div class="backers">
                        <h3><?php tpl_link(wl(),$conf['title'],'accesskey="h" title="[H]"') ?></h3>
                        <?php /* how to insert logo instead (if no CSS image replacement technique is used):
                        upload your logo into the data/media folder (root of the media manager) and replace 'logo.png' accordingly:
                        tpl_link(wl(),'<img src="'.ml('logo.png').'" alt="'.$conf['title'].'" />','id="dokuwiki__top" accesskey="h" title="[H]"') */ ?>
                        <!-- <span>backers</span> -->
                        
                        <?php if ($conf['tagline']): ?>
                            <span class="claim"><?php echo $conf['tagline'] ?></span>
                        <?php endif ?>

                        <ul class="a11y skip">
                            <li><a href="#dokuwiki__content"><?php echo $lang['skip_to_content'] ?></a></li>
                        </ul>
                    </div>
                    <!-- <div class="funded">
                        <h3>$10,350</h3>
                        <span>raised out of $23,000</span>
                    </div>
                    <div class="time-left">
                        <h3>27</h3>
                        <span>days left to go</span>
                    </div> -->
                    <div class="reminder last">
                        <!-- <a href="#"><i class="fa fa-star"></i> REMIND ME</a> -->
                        <?php tpl_searchform() ?>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--/header-->
    <!--main content-->
    <div class="main-content">
        <div class="container">
            <div class="row">
                <div class="content col-md-8 col-sm-12 col-xs-12">

                    <?php html_msgarea() /* occasional error and info messages on top of the page */ ?>

                    <!-- <div class="section-block">
                        <div class="funding-meta">
                            <h1>LAUNCH INTO SUCCESS</h1>
                            <span class="type-meta"><i class="fa fa-user"></i> Jonathan Doe</span>
                            <span class="type-meta"><i class="fa fa-tag"></i> <a href="#">crowdfunding</a>, <a href="#">launch</a> </span>
                            <img src="assets/img/image-heartbeat.jpg" class="img-responsive" alt="launch HTML5 Crowdfunding">
                            <div class="video-frame">
                                <iframe src="https://player.vimeo.com/video/67938315" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                            </div>
                            <p>Launch will enable you be in run  your own crowdfunding campaign, and be in complete control from concept to crowdfunding to launch.</p>
                            <h2>$10,350</h2>                            
                            <span class="contribution">raised by <strong>5,234</strong> ready to launch</span>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%;">
                                    <span class="sr-only">45% Complete</span>
                                </div>
                            </div>
                            <span class="goal-progress"><strong>45%</strong> of $23,000 raised</span>
                        </div>
                        <span class="count-down"><strong>27</strong>Days to go.</span>
                        <a href="#" class="btn btn-launch">HELP LAUNCH</a>
                    </div> -->
                    <!--signup-->
                    <!--
                    <div class="section-block signup">
                        <div class="sign-up-form">
                            <form>
                                <p>Sign up now for updates and a chance to win a free version of launch!</p>
                                <input class="signup-input" type="text" name="email" placeholder="Email Address"><button class="btn btn-signup" type="submit"><i class="fa fa-paper-plane"></i></button>
                            </form>
                        </div>
                    </div> -->
                    <!--/signup-->
                    <!--tabs-->
                    <!-- <div class="section-block">
                        <div class="section-tabs">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#about" aria-controls="about" role="tab" data-toggle="tab">About</a></li>
                                <li role="presentation"><a href="#updates" aria-controls="updates" role="tab" data-toggle="tab">Updates</a></li>
                            </ul>
                        </div>
                    </div> -->
                    <!--/tabs-->
                    <!--tab panes-->
                    <div class="section-block">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="about">
                                <div class="about-information">

                                    <!-- BREADCRUMBS -->
                                    <?php if($conf['breadcrumbs']){ ?>
                                        <div class="breadcrumbs"><?php tpl_breadcrumbs() ?></div>
                                    <?php } ?>
                                    <?php if($conf['youarehere']){ ?>
                                        <div class="breadcrumbs"><?php tpl_youarehere() ?></div>
                                    <?php } ?>

                                    <!-- <h1 class="section-title">ABOUT LAUNCH</h1> -->
                                    <!-- ********** CONTENT ********** -->
                                    <div id="dokuwiki__content"><div class="pad">
                                        <?php tpl_flush() /* flush the output buffer */ ?>
                                        <?php tpl_includeFile('pageheader.html') ?>

                                        <div class="page">
                                            <!-- wikipage start -->
                                            <?php tpl_content() /* the main content */ ?>
                                            <!-- wikipage stop -->
                                            <div class="clearer"></div>
                                        </div>

                                        <?php tpl_flush() ?>
                                        <?php tpl_includeFile('pagefooter.html') ?>
                                    </div></div><!-- /content -->
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="updates">
                                <div class="update-information">
                                <h1 class="section-title">UPDATES</h1>
                                    <!--update items-->
                                    <div class="update-post">
                                        <h4 class="update-title">We've started shipping!</h4>
                                        <span class="update-date">Posted 2 days ago</span>
                                        <p>Suspendisse luctus at massa sit amet bibendum. Cras commodo congue urna, vel dictum velit bibendum eget. Vestibulum quis risus euismod, facilisis lorem nec, dapibus leo. Quisque sodales eget dolor iaculis dapibus. Vivamus sit amet lacus ipsum. Nullam varius lobortis neque, et efficitur lacus. Quisque dictum tellus nec mi luctus imperdiet. Morbi vel aliquet velit, accumsan dapibus urna. Cras ligula orci, suscipit id eros non, rhoncus efficitur nisi.</p>
                                    </div>
                                    <div class="update-post">
                                        <h4 class="update-title">Launch begins manufacturing </h4>
                                        <span class="update-date">Posted 9 days ago</span>
                                        <p>Suspendisse luctus at massa sit amet bibendum. Cras commodo congue urna, vel dictum velit bibendum eget. Vestibulum quis risus euismod, facilisis lorem nec, dapibus leo. Quisque sodales eget dolor iaculis dapibus. Vivamus sit amet lacus ipsum. Nullam varius lobortis neque, et efficitur lacus. Quisque dictum tellus nec mi luctus imperdiet. Morbi vel aliquet velit, accumsan dapibus urna. Cras ligula orci, suscipit id eros non, rhoncus efficitur nisi.</p>
                                    </div>
                                    <div class="update-post">
                                        <h4 class="update-title">Designs have now been finalized</h4>
                                        <span class="update-date">Posted 17 days ago</span>
                                        <p>Suspendisse luctus at massa sit amet bibendum. Cras commodo congue urna, vel dictum velit bibendum eget. Vestibulum quis risus euismod, facilisis lorem nec, dapibus leo. Quisque sodales eget dolor iaculis dapibus. Vivamus sit amet lacus ipsum. Nullam varius lobortis neque, et efficitur lacus. Quisque dictum tellus nec mi luctus imperdiet. Morbi vel aliquet velit, accumsan dapibus urna. Cras ligula orci, suscipit id eros non, rhoncus efficitur nisi.</p>
                                    </div>
                                    <!--/update items-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/tabs-->
                <!--/main content-->
                <!--sidebar-->
                <div class="content col-md-4 col-sm-12 col-xs-12">
                    <!-- <div class="section-block summary">
                        <h1 class="section-title">LAUNCH</h1>
                        <div class="profile-contents">
                            <h2 class="position">Sky Rocketing Your Funding Campaign</h2>
                            <img src="<?php echo tpl_basedir();?>/images/profile-img.jpg" class="profile-image img responsive" alt="John Doe">
                            <!--social links-->
                            <!--
                            <ul class="list-inline">
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-git"></i></a></li>
                            </ul>
                            <!--/social links-->
                            <!--
                            <a href="#" class="btn btn-contact"><i class="fa fa-envelope"></i>CONTACT US</a>
                        </div>
                    </div> -->
                    <!--credits-->
                    <?php if ($showSidebar): ?>
                    <div class="section-block">
                        <!-- <h1 class="section-title">Site Tools</h1> -->
                        <!--credits block-->
                        <!-- <div class="credit-block sources"> -->
                          <!-- ********** ASIDE ********** -->
                            <div id="writtensidebar">
                                <?php tpl_includeFile('sidebarheader.html') ?>
                                <?php tpl_include_page($conf['sidebar'], 1, 1) /* includes the nearest sidebar page */ ?>
                                <?php tpl_includeFile('sidebarfooter.html') ?>
                                <div class="clearer"></div>
                            </div><!-- /aside -->
                        <!-- </div> -->
                    </div>
                    <?php endif; ?>
                    <div class="section-block">
                        <h1 class="section-title">Site Tools</h1>
                        <!--credits block-->
                        <div class="credit-block sources">
                            <ul class="list-unstyled">
                                <!-- SITE TOOLS -->
                                <h3 class="a11y"><?php echo $lang['site_tools'] ?></h3>
                                    <?php tpl_toolsevent('sitetools', array(
                                        'recent'    => tpl_action('recent', 1, 'li', 1),
                                        'media'     => tpl_action('media', 1, 'li', 1),
                                        'index'     => tpl_action('index', 1, 'li', 1),
                                    )); ?>
                            </ul>
                        </div>
                    </div>
                    <?php if ($conf['useacl'] && $showTools): ?>
                    <div class="section-block">
                        <h1 class="section-title">User Tools</h1>
                        <div class="credit-block sources">
                            <ul class="list-unstyled">
                                <!-- USER TOOLS -->
                                <h3 class="a11y"><?php echo $lang['user_tools'] ?></h3>
                                    <?php
                                        if (!empty($_SERVER['REMOTE_USER'])) {
                                            echo '<li class="user">';
                                            tpl_userinfo(); /* 'Logged in as ...' */
                                            echo '</li>';
                                        }
                                    ?>
                                    <?php /* the optional second parameter of tpl_action() switches between a link and a button,
                                             e.g. a button inside a <li> would be: tpl_action('edit', 0, 'li') */
                                    ?>
                                    <?php tpl_toolsevent('usertools', array(
                                        'admin'     => tpl_action('admin', 1, 'li', 1),
                                        'userpage'  => _tpl_action('userpage', 1, 'li', 1),
                                        'profile'   => tpl_action('profile', 1, 'li', 1),
                                        'register'  => tpl_action('register', 1, 'li', 1),
                                        'login'     => tpl_action('login', 1, 'li', 1),
                                    )); ?>
                            </ul>
                        </div>
                    </div>
                    <?php endif ?>
                    <?php if ($showTools): ?>
                    <div class="section-block">
                        <h1 class="section-title">Page Tools</h1>
                        <div class="credit-block sources">
                            <ul class="list-unstyled">
                                 <!-- PAGE ACTIONS -->
                                <h3 class="a11y"><?php echo $lang['page_tools'] ?></h3>
                                    <?php tpl_toolsevent('pagetools', array(
                                        'edit'      => tpl_action('edit', 1, 'li', 1),
                                        'discussion'=> _tpl_action('discussion', 1, 'li', 1),
                                        'revisions' => tpl_action('revisions', 1, 'li', 1),
                                        'backlink'  => tpl_action('backlink', 1, 'li', 1),
                                        'subscribe' => tpl_action('subscribe', 1, 'li', 1),
                                        'revert'    => tpl_action('revert', 1, 'li', 1),
                                        'top'       => tpl_action('top', 1, 'li', 1),
                                    )); ?>
                            </ul>
                        </div>
                        <?php endif; ?>
                        <!--/credits block-->
                    </div>
                    <!--/credits-->
                </div>
                <!--/sidebar-->
            </div>
        </div>
        <div class="no"><?php tpl_indexerWebBug() /* provide DokuWiki housekeeping, required in all templates */ ?></div>
    </div>
    <footer class="footer">
    <div class="container">
            <div class="row">
                <!--This template has been created under the Creative Commons Attribution 3.0 License. Please keep the attribution link below when using this template in your own project, thank you.-->
                <span class="copyright">Created by <a href="http://themes.audaindesigns.com" target="_blank">Audain Designs</a> for free use and converted by <a href="http://desbest.com" target="_blank">desbest</a></span>
                <div class="doc"><?php tpl_pageinfo() /* 'Last modified' etc */ ?></div>
                <?php tpl_license('button') /* content license, parameters: img=*badge|button|0, imgonly=*0|1, return=*0|1 */ ?>
            </div>
        </div>

    </footer>
    <?php tpl_includeFile('footer.html') ?>

    <?php /* with these Conditional Comments you can better address IE issues in CSS files,
             precede CSS rules by #IE8 for IE8 (div closes at the bottom) */ ?>
    <!--[if lte IE 8 ]><div id="IE8"><![endif]-->

    <?php /* the "dokuwiki__top" id is needed somewhere at the top, because that's where the "back to top" button/link links to */ ?>
    <?php /* tpl_classes() provides useful CSS classes; if you choose not to use it, the 'dokuwiki' class at least
             should always be in one of the surrounding elements (e.g. plugins and templates depend on it) */ ?>
 
    
    <!--[if lte IE 8 ]></div><![endif]-->
</body>
</html>
